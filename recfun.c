#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "prescheme.h"

struct vec2 {
  long x;
  long y;
};
struct rect {
  struct vec2 *tl;
  struct vec2 *wh;
};
long write_vec2(struct vec2*, FILE *);
long write_rect(struct rect*, FILE *);
long main(void);


long write_vec2(struct vec2 *a_0X, FILE * port_1X)
{
  long v_2X;
 {  ps_write_string("#<vec2 ", port_1X);
  ps_write_integer((a_0X->x), port_1X);
  { long ignoreXX;
  PS_WRITE_CHAR(32, port_1X, ignoreXX) }
  ps_write_integer((a_0X->y), port_1X);
  PS_WRITE_CHAR(62, port_1X, v_2X)
  return v_2X;}
}
long write_rect(struct rect *a_3X, FILE * port_4X)
{
  long v_5X;
 {  ps_write_string("#<rect ", port_4X);
  ps_write_integer(((a_3X->tl)->x), port_4X);
  { long ignoreXX;
  PS_WRITE_CHAR(32, port_4X, ignoreXX) }
  ps_write_integer(((a_3X->tl)->y), port_4X);
  { long ignoreXX;
  PS_WRITE_CHAR(32, port_4X, ignoreXX) }
  ps_write_integer(((a_3X->wh)->x), port_4X);
  { long ignoreXX;
  PS_WRITE_CHAR(32, port_4X, ignoreXX) }
  ps_write_integer(((a_3X->wh)->y), port_4X);
  PS_WRITE_CHAR(62, port_4X, v_5X)
  return v_5X;}
}
long main(void)
{
  struct rect *arg1K0;
  struct vec2 *arg0K0;
  FILE * merged_arg3K2;
  struct vec2 *merged_arg0K1;
  char *merged_arg2K0;

#ifdef USE_DIRECT_THREADING
  void *write_cornerD0_return_address;
#else
  int write_cornerD0_return_tag;
#endif
  char *name_6X;
  struct vec2 *corner_7X;
  FILE * out_8X;
  struct vec2 *v_31X;
  struct vec2 *vec2_30X;
  long y_29X;
  long x_28X;
  struct vec2 *v_27X;
  struct vec2 *vec2_26X;
  long y_25X;
  long x_24X;
  struct vec2 *v_23X;
  struct vec2 *vec2_22X;
  long y_21X;
  long x_20X;
  struct vec2 *v_19X;
  struct vec2 *vec2_18X;
  long y_17X;
  long x_16X;
  struct rect *a_15X;
  struct rect *rect_14X;
  struct vec2 *wh_13X;
  struct vec2 *vec2_12X;
  struct vec2 *tl_11X;
  struct vec2 *vec2_10X;
  FILE * out_9X;
 {  out_9X = stdout;
  vec2_10X = (struct vec2*)malloc(sizeof(struct vec2));
  if ((NULL == vec2_10X)) {
    arg0K0 = vec2_10X;
    goto L389;}
  else {
    vec2_10X->x = 10;
    vec2_10X->y = 10;
    arg0K0 = vec2_10X;
    goto L389;}}
 L389: {
  tl_11X = arg0K0;
  vec2_12X = (struct vec2*)malloc(sizeof(struct vec2));
  if ((NULL == vec2_12X)) {
    arg0K0 = vec2_12X;
    goto L391;}
  else {
    vec2_12X->x = 2;
    vec2_12X->y = 2;
    arg0K0 = vec2_12X;
    goto L391;}}
 L391: {
  wh_13X = arg0K0;
  rect_14X = (struct rect*)malloc(sizeof(struct rect));
  if ((NULL == rect_14X)) {
    arg1K0 = rect_14X;
    goto L393;}
  else {
    rect_14X->tl = tl_11X;
    rect_14X->wh = wh_13X;
    arg1K0 = rect_14X;
    goto L393;}}
 L393: {
  a_15X = arg1K0;write_rect(a_15X, out_9X);
  { long ignoreXX;
  PS_WRITE_CHAR(10, out_9X, ignoreXX) }
  x_16X = (a_15X->tl)->x;
  y_17X = (a_15X->tl)->y;
  vec2_18X = (struct vec2*)malloc(sizeof(struct vec2));
  if ((NULL == vec2_18X)) {
    arg0K0 = vec2_18X;
    goto L417;}
  else {
    vec2_18X->x = x_16X;
    vec2_18X->y = y_17X;
    arg0K0 = vec2_18X;
    goto L417;}}
 L417: {
  v_19X = arg0K0;
  merged_arg2K0 = "top-left";
  merged_arg0K1 = v_19X;
  merged_arg3K2 = out_9X;
#ifdef USE_DIRECT_THREADING
  write_cornerD0_return_address = &&write_cornerD0_return_0;
#else
  write_cornerD0_return_tag = 0;
#endif
  goto write_cornerD0;
 write_cornerD0_return_0:
  x_20X = ((a_15X->tl)->x) + ((a_15X->wh)->x);
  y_21X = (a_15X->tl)->y;
  vec2_22X = (struct vec2*)malloc(sizeof(struct vec2));
  if ((NULL == vec2_22X)) {
    arg0K0 = vec2_22X;
    goto L421;}
  else {
    vec2_22X->x = x_20X;
    vec2_22X->y = y_21X;
    arg0K0 = vec2_22X;
    goto L421;}}
 L421: {
  v_23X = arg0K0;
  merged_arg2K0 = "top-right";
  merged_arg0K1 = v_23X;
  merged_arg3K2 = out_9X;
#ifdef USE_DIRECT_THREADING
  write_cornerD0_return_address = &&write_cornerD0_return_1;
#else
  write_cornerD0_return_tag = 1;
#endif
  goto write_cornerD0;
 write_cornerD0_return_1:
  x_24X = (a_15X->tl)->x;
  y_25X = ((a_15X->tl)->y) + ((a_15X->wh)->y);
  vec2_26X = (struct vec2*)malloc(sizeof(struct vec2));
  if ((NULL == vec2_26X)) {
    arg0K0 = vec2_26X;
    goto L425;}
  else {
    vec2_26X->x = x_24X;
    vec2_26X->y = y_25X;
    arg0K0 = vec2_26X;
    goto L425;}}
 L425: {
  v_27X = arg0K0;
  merged_arg2K0 = "bottom-left";
  merged_arg0K1 = v_27X;
  merged_arg3K2 = out_9X;
#ifdef USE_DIRECT_THREADING
  write_cornerD0_return_address = &&write_cornerD0_return_2;
#else
  write_cornerD0_return_tag = 2;
#endif
  goto write_cornerD0;
 write_cornerD0_return_2:
  x_28X = ((a_15X->tl)->x) + ((a_15X->wh)->x);
  y_29X = ((a_15X->tl)->y) + ((a_15X->wh)->y);
  vec2_30X = (struct vec2*)malloc(sizeof(struct vec2));
  if ((NULL == vec2_30X)) {
    arg0K0 = vec2_30X;
    goto L429;}
  else {
    vec2_30X->x = x_28X;
    vec2_30X->y = y_29X;
    arg0K0 = vec2_30X;
    goto L429;}}
 L429: {
  v_31X = arg0K0;
  merged_arg2K0 = "bottom-right";
  merged_arg0K1 = v_31X;
  merged_arg3K2 = out_9X;
#ifdef USE_DIRECT_THREADING
  write_cornerD0_return_address = &&write_cornerD0_return_3;
#else
  write_cornerD0_return_tag = 3;
#endif
  goto write_cornerD0;
 write_cornerD0_return_3:
  return 0;}
 write_cornerD0: {
  name_6X = merged_arg2K0;
  corner_7X = merged_arg0K1;
  out_8X = merged_arg3K2;{
  ps_write_string(name_6X, out_8X);
  ps_write_string(": ", out_8X);write_vec2(corner_7X, out_8X);
  { long ignoreXX;
  PS_WRITE_CHAR(10, out_8X, ignoreXX) }
  free(corner_7X);
#ifdef USE_DIRECT_THREADING
  goto *write_cornerD0_return_address;
#else
  goto write_cornerD0_return;
#endif
}
#ifndef USE_DIRECT_THREADING
 write_cornerD0_return:
  switch (write_cornerD0_return_tag) {
  case 0: goto write_cornerD0_return_0;
  case 1: goto write_cornerD0_return_1;
  case 2: goto write_cornerD0_return_2;
  default: goto write_cornerD0_return_3;
  }
#endif
}

}
