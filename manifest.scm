(use-modules (guix gexp)
             ((guix licenses) #:select (bsd-3))
             (guix packages)
             (guix profiles)
             (gnu packages base)
             (gnu packages commencement)
             (gnu packages pkg-config)
             (gnu packages scheme))

(packages->manifest
 (list scheme48-prescheme
       gcc-toolchain
       gnu-make
       pkg-config))
