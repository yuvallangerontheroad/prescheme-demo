(define-structure ps-string
  (export string-copy!
          string-append
          string-repeat)
  (open prescheme)
  (files (lib ps-string)))

(define-structure ps-vector
  (export (vector-unfold :syntax) vector-unfold1 vector-unfold2 vector-unfold3
          (vector-fold :syntax) vector-fold1 vector-fold2 vector-fold3
          (vector-map! :syntax) vector-map1! vector-map2! vector-map3!
          (vector-map :syntax) vector-map1 vector-map2 vector-map3
          (vector-for-each :syntax) vector-for-each1 vector-for-each2 vector-for-each3)
  (open prescheme)
  (open ps-receive)
  (files (lib ps-vector)))

(define-structure hello (export main)
  (open prescheme)
  (files hello))

(define-structure append (export main string-append)
  (open prescheme)
  (open ps-string)
  (files append))

(define-structure vecfun (export main)
  (open prescheme)
  (open ps-string)
  (open ps-vector)
  (files vecfun))

(define-structure recfun (export main write-vec2 write-rect)
  (open prescheme)
  (open ps-record-types)
  (files recfun))

(define-structure btree (export main)
  (open prescheme)
  (open ps-record-types)
  (files btree))
