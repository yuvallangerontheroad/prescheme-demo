#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "prescheme.h"

static void string_copyB(char*, long, char*, long, long);
char *string_append(char*, char*);
long main(long, char**);


static void string_copyB(char *target_0X, long offset_1X, char *source_2X, long start_3X, long end_4X)
{
  long arg0K1;
  long arg0K0;
  long src_6X;
  long tgt_5X;
 {  arg0K0 = offset_1X;
  arg0K1 = start_3X;
  goto L18;}
 L18: {
  tgt_5X = arg0K0;
  src_6X = arg0K1;
  if ((src_6X == end_4X)) {
    return;}
  else {
    *(target_0X + tgt_5X) = (*(source_2X + src_6X));
    arg0K0 = (1 + tgt_5X);
    arg0K1 = (1 + src_6X);
    goto L18;}}
}
char *string_append(char *a_7X, char *b_8X)
{
  char *target_11X;
  long len_b_10X;
  long len_a_9X;
 {  len_a_9X = strlen((char *) a_7X);
  len_b_10X = strlen((char *) b_8X);
  target_11X = (char *)calloc( 1, 1 + (len_a_9X + len_b_10X));string_copyB(target_11X, 0, a_7X, 0, len_a_9X);string_copyB(target_11X, len_a_9X, b_8X, 0, len_b_10X);
  return target_11X;}
}
long main(long argc_12X, char **argv_13X)
{
  FILE * merged_arg2K2;
  char *merged_arg1K1;
  char *merged_arg1K0;

#ifdef USE_DIRECT_THREADING
  void *demo_string_append_return_address;
#else
  int demo_string_append_return_tag;
#endif
  char demo_string_append0_return_value;
  char *a_14X;
  char *b_15X;
  FILE * out_16X;
  char *target_23X;
  long len_b_22X;
  long len_a_21X;
  FILE * out_20X;
  char *b_19X;
  char *a_18X;
  FILE * out_17X;
 {  if ((3 == argc_12X)) {
    out_17X = stdout;
    a_18X = *(argv_13X + 1);
    b_19X = *(argv_13X + 2);
    merged_arg1K0 = a_18X;
    merged_arg1K1 = a_18X;
    merged_arg2K2 = out_17X;
#ifdef USE_DIRECT_THREADING
    demo_string_append_return_address = &&demo_string_append_return_0;
#else
    demo_string_append_return_tag = 0;
#endif
    goto demo_string_append;
   demo_string_append_return_0:
    merged_arg1K0 = a_18X;
    merged_arg1K1 = b_19X;
    merged_arg2K2 = out_17X;
#ifdef USE_DIRECT_THREADING
    demo_string_append_return_address = &&demo_string_append_return_1;
#else
    demo_string_append_return_tag = 1;
#endif
    goto demo_string_append;
   demo_string_append_return_1:
    merged_arg1K0 = b_19X;
    merged_arg1K1 = b_19X;
    merged_arg2K2 = out_17X;
#ifdef USE_DIRECT_THREADING
    demo_string_append_return_address = &&demo_string_append_return_2;
#else
    demo_string_append_return_tag = 2;
#endif
    goto demo_string_append;
   demo_string_append_return_2:
    return 0;}
  else {
    out_20X = stderr;
    ps_write_string("Usage: ", out_20X);
    ps_write_string((*(argv_13X + 0)), out_20X);
    ps_write_string(" <string-a> <string-b>", out_20X);
    { long ignoreXX;
    PS_WRITE_CHAR(10, out_20X, ignoreXX) }
    ps_write_string("  Prints permutations of <string-a> and <string-b>.", out_20X);
    { long ignoreXX;
    PS_WRITE_CHAR(10, out_20X, ignoreXX) }
    return 1;}}
 demo_string_append: {
  a_14X = merged_arg1K0;
  b_15X = merged_arg1K1;
  out_16X = merged_arg2K2;{
  len_a_21X = strlen((char *) a_14X);
  len_b_22X = strlen((char *) b_15X);
  target_23X = (char *)calloc( 1, 1 + (len_a_21X + len_b_22X));string_copyB(target_23X, 0, a_14X, 0, len_a_21X);string_copyB(target_23X, len_a_21X, b_15X, 0, len_b_22X);
  ps_write_string(target_23X, out_16X);
  { long ignoreXX;
  PS_WRITE_CHAR(10, out_16X, ignoreXX) }
  free(target_23X);
  demo_string_append0_return_value = 1;
#ifdef USE_DIRECT_THREADING
  goto *demo_string_append_return_address;
#else
  goto demo_string_append_return;
#endif
}
#ifndef USE_DIRECT_THREADING
 demo_string_append_return:
  switch (demo_string_append_return_tag) {
  case 0: goto demo_string_append_return_0;
  case 1: goto demo_string_append_return_1;
  default: goto demo_string_append_return_2;
  }
#endif
}

}
