# Pre-Scheme Makefile

CC=gcc
PRESCHEME=prescheme

CFLAGS=-g -Wall
CFLAGS+=$(shell pkg-config --cflags prescheme)
LDLIBS+=$(shell pkg-config --libs prescheme)

SOURCES= packages.scm \
         lib/ps-string.scm \
         lib/ps-vector.scm

TARGETS= hello \
         append \
         vecfun \
         recfun \
         btree

all: $(TARGETS)

%.c: %.scm $(SOURCES)
	rm -f $@
	( echo ",batch"; \
	  echo "(prescheme-compiler '$* '(\"packages.scm\") '$*-init \"$@\")"; \
	  echo ",exit" ) \
	| $(PRESCHEME)

clean:
	rm -f $(TARGETS)
	rm -f $(TARGETS:=.o)
	rm -f $(TARGETS:=.c)

.PRECIOUS: $(TARGETS:=.c)

.PHONY: all clean
